import rootReducer from "../reducers";
import { createStore } from "redux";
import { themes } from "../ressources/themes";
import createThemeCSS from "../ressources/createThemeCSS";

export default (initialState = null) => {
  return createStore(rootReducer, populateState(initialState));
};

function populateState(state) {
  const newState = { ...state };
  newState.colorThemes = newState.colorThemes || { themes, createThemeCSS };
  if (newState.colorThemes.selectedThemeId) {
    const selectedTheme = themes.find(
      t => t.id === newState.colorThemes.selectedThemeId
    );
    if (!selectedTheme) {
      newState.colorThemes.selectedThemeId = null;
    } else {
      newState.colorThemes.colors = selectedTheme.colors;
    }
  }
  if (!newState.colorThemes.colors) {
    const selectedTheme = themes[0];
    newState.colorThemes.selectedThemeId = selectedTheme.id;
    newState.colorThemes.colors = selectedTheme.colors;
  }
  return newState;
}
