import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import App from "./components/app/App";
import configureStore from "./store/configure-store";
import "bootstrap/dist/css/bootstrap.css";
import "./index.css";

const store = configureStore();

ReactDOM.render(
  <div className="main">
    <Provider store={store}>
      <App />
    </Provider>
  </div>,
  document.getElementById("root")
);
