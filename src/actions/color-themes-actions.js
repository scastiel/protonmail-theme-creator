import * as types from "./action-types";

export const changeTheme = theme => ({
  type: types.CHANGE_THEME,
  selectedThemeId: theme.id
});

export const changeColors = colors => ({
  type: types.CHANGE_COLORS,
  colors: { ...colors }
});
