import * as types from "../actions/action-types";

export default function colorThemesReducer(state = {}, action) {
  switch (action.type) {
    case types.CHANGE_THEME: {
      const selectedThemeId = action.selectedThemeId;
      const colors = {
        ...state.themes.find(t => t.id === selectedThemeId).colors
      };
      return { ...state, selectedThemeId, colors };
    }
    case types.CHANGE_COLORS: {
      const selectedThemeId = null;
      const colors = { ...action.colors };
      return { ...state, selectedThemeId, colors };
    }
    default: {
      return state;
    }
  }
}
