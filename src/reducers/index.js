import { combineReducers } from "redux";
import colorThemesReducer from "./color-themes-reducer";

const rootReducer = combineReducers({
  colorThemes: colorThemesReducer
});

export default rootReducer;
