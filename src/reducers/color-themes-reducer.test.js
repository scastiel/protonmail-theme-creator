import { expect } from "chai";
import * as types from "../actions/action-types";
import colorThemesReducer from "./color-themes-reducer";

const themes = [
  { id: "theme1", name: "Theme #1", colors: { color1: "#123 " } },
  { id: "theme2", name: "Theme #2", colors: { color1: "#456 " } }
];

it("should change theme", () => {
  const state = {
    themes,
    selectedThemeId: themes[0].id,
    colors: themes[0].colors
  };
  const action = { type: types.CHANGE_THEME, selectedThemeId: themes[1].id };
  const newState = colorThemesReducer(state, action);
  expect(newState.selectedThemeId).to.equal(themes[1].id);
  expect(newState.colors).to.deep.equal(themes[1].colors);
});

it("should change colors", () => {
  const state = {
    themes,
    selectedThemeId: themes[0].id,
    colors: themes[0].colors
  };
  const action = { type: types.CHANGE_COLORS, colors: { color1: "#000" } };
  const newState = colorThemesReducer(state, action);
  expect(newState.selectedThemeId).to.be.null;
  expect(newState.colors).to.deep.equal({ color1: "#000" });
});

it("should not change anything if other action", () => {
  const state = {
    themes,
    selectedThemeId: themes[0].id,
    colors: themes[0].colors
  };
  const action = { type: "unknown action" };
  const newState = colorThemesReducer(state, action);
  expect(newState).to.deep.equal(state);
});
