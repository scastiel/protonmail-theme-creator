export const colorDefinitions = [
  { id: "primaryColor", name: "Primary color" },
  { id: "backgroundColor", name: "Background color" },
  { id: "conversationsBackgroundColor", name: "Conversations background" },
  { id: "borderColor", name: "Border color" },
  { id: "composeButtonBackgroundColor", name: "Compose button background" },
  { id: "textColor", name: "Text color" }
];

export const themes = [
  {
    id: "google",
    name: "Google",
    colors: {
      primaryColor: "#4285f4",
      backgroundColor: "#f2f2f2",
      conversationsBackgroundColor: "#ffffff",
      borderColor: "#e0e0e0",
      composeButtonBackgroundColor: "#d23f31",
      textColor: "#616161"
    }
  },
  {
    id: "darkProton",
    name: "Dark Proton",
    colors: {
      primaryColor: "#2F2F38",
      backgroundColor: "#505061",
      conversationsBackgroundColor: "#555962",
      borderColor: "#666870",
      composeButtonBackgroundColor: "#797BA3",
      textColor: "#B2B2B2"
    }
  },
  {
    id: "detchen",
    name: "Detchen",
    colors: {
      primaryColor: "#1C3144",
      backgroundColor: "#EAEAEA",
      conversationsBackgroundColor: "#FFFFFF",
      borderColor: "#e0e0e0",
      composeButtonBackgroundColor: "#004BA8",
      textColor: "#616161"
    }
  },
  {
    id: "fornax",
    name: "Fornax",
    colors: {
      primaryColor: "#005DB3",
      backgroundColor: "#F7F0F5",
      conversationsBackgroundColor: "#FFFFFF",
      borderColor: "#e0e0e0",
      composeButtonBackgroundColor: "#B3005D",
      textColor: "#616161"
    }
  },
  {
    id: "proton",
    name: "Proton",
    colors: {
      primaryColor: "#505061",
      backgroundColor: "#f2f2f2",
      conversationsBackgroundColor: "#e6eaf0",
      borderColor: "#acb0bf",
      composeButtonBackgroundColor: "#9397cd",
      textColor: "#616161"
    }
  },
  {
    id: "reddy",
    name: "Reddy",
    colors: {
      primaryColor: "#D62828",
      backgroundColor: "#EAEAEA",
      conversationsBackgroundColor: "#FFFFFF",
      borderColor: "#e0e0e0",
      composeButtonBackgroundColor: "#003049",
      textColor: "#616161"
    }
  }
];

export const previewParameters = {
  labels: ["Inbox", "Sent", "Archive"],
  menus: ["Settings", "Me"],
  toolbarButtons: ["", "", ""],
  conversations: [
    {
      title: "One very interesting conversation",
      unread: true,
      active: false,
      starred: false
    },
    {
      title: "Another good conversation",
      unread: false,
      active: true,
      starred: true
    },
    {
      title: "A not that good conversation",
      unread: false,
      active: false,
      starred: false
    }
  ],
  composeButtonTitle: "Compose"
};
