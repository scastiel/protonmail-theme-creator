import customCSS from "!raw!./theme-css/custom.css";
import pmThemeCSS from "!raw!./theme-css/pm-theme.css";

export default function createThemeCSS(colors) {
  const allColors = {
    ...colors,
    red: "#d23f31",
    redLight: "#de5245",
    yellow: "#fdd835",
    yellowLight: "#fde475",
    green: "#0f9d58",
    greenLight: "#89bc70",
    blueLight: "#93b9cc",
    white: "#FFF",
    light1: "#e5e5e5",
    light2: "#fafafa",
    light3: "#F5F5F5",
    light4: "#f4f4f4",
    medium1: "#CCC",
    medium2: "#8c8c8c",
    dark0: "#333",
    dark1: "#444",
    dark2: colors.textColor,
    dark3: "#666",
    dark4: "#222",
    black: "#000",
    theme1: colors.primaryColor,
    theme2: colors.backgroundColor,
    theme3: colors.conversationsBackgroundColor,
    theme4: colors.borderColor,
    theme5: colors.primaryColor,
    theme6: colors.primaryColor,
    theme7: colors.primaryColor
  };

  const regExpFromColorName = colorName =>
    new RegExp(`var\\(--${colorName}\\)`, "g");
  return Object.keys(allColors).reduce(
    (acc, colorName) =>
      acc.replace(regExpFromColorName(colorName), allColors[colorName]),
    `${customCSS} ${pmThemeCSS}`
  );
}
