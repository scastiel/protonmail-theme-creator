import React from "react";
import ReactDOM from "react-dom";
import { expect } from "chai";
import ReactTestUtils from "react-addons-test-utils";
import ThemeSelector from "./ThemeSelector";

const themes = [
  { id: "theme1", name: "Theme #1", colors: {} },
  { id: "theme2", name: "Theme #2", colors: {} }
];

it("should list themes", () => {
  const div = document.createElement("div");
  ReactDOM.render(<ThemeSelector themes={themes} />, div);
  const themeElements = div.querySelectorAll(".theme");
  expect(
    Array.from(themeElements).map(th => th.querySelector("a").innerHTML)
  ).to.deep.equal(themes.map(t => t.name));
});

it("should highlight selected theme", () => {
  const div = document.createElement("div");
  ReactDOM.render(<ThemeSelector themes={themes} value={themes[0].id} />, div);
  const selectedThemeElements = div.querySelectorAll(".theme a.active");
  expect(
    Array.from(selectedThemeElements).map(link => link.innerHTML)
  ).to.deep.equal([themes[0].name]);
});

it("should call onChange when clicking on a theme", () => {
  const onChange = jest.fn();
  const div = document.createElement("div");
  ReactDOM.render(
    <ThemeSelector themes={themes} changeTheme={onChange} />,
    div
  );
  const themeLink = div.querySelector(".theme:nth-of-type(1) a");
  ReactTestUtils.Simulate.click(themeLink);
  expect(onChange.mock.calls).to.have.lengthOf(1);
  expect(onChange.mock.calls[0][0]).to.equal(themes[0]);
});
