import React, { Component, PropTypes } from "react";
import "./ThemeSelector.css";

export default class ThemeSelector extends Component {
  static propTypes = {
    themes: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string,
        name: PropTypes.string,
        colors: PropTypes.objectOf(PropTypes.string)
      })
    ),
    value: PropTypes.string,
    changeTheme: PropTypes.func
  };
  onThemeChanged(event, theme) {
    event.preventDefault();
    if (typeof this.props.changeTheme === "function") {
      this.props.changeTheme(theme);
    }
  }
  render() {
    return (
      <p className="theme-selector">
        Select one of the predefined themes:<br />
        {this.props.themes.map(theme => (
          <span className="theme" key={theme.id}>
            <a
              href="#"
              onClick={e => this.onThemeChanged(e, theme)}
              className={this.props.value === theme.id ? "active" : ""}
            >
              {theme.name}
            </a>
            &nbsp;
          </span>
        ))}
      </p>
    );
  }
}
