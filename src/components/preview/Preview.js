import React, { Component } from "react";
import "./Preview.css";

class Preview extends Component {
  render() {
    return (
      <div className="preview">
        <div
          className="header"
          style={{ backgroundColor: this.props.colors.primaryColor }}
        >
          <div className="logo" />
          <div className="search">
            <div className="border left" />
            <div className="search-field" />
            <div className="border right" />
          </div>
          <ul className="menus">
            {this.props.params.menus.map((menu, menuIndex) => (
              <li
                key={menuIndex}
                style={{
                  color: this.props.colors.conversationsBackgroundColor
                }}
              >
                <span className="icon" />
                {menu}
              </li>
            ))}
          </ul>
        </div>
        <div
          className="content"
          style={{ backgroundColor: this.props.colors.backgroundColor }}
        >
          <div className="sidebar">
            <div
              className="compose-button"
              style={{
                backgroundColor: this.props.colors.composeButtonBackgroundColor
              }}
            >
              {this.props.params.composeButtonTitle}
            </div>
            <ul className="labels">
              {this.props.params.labels.map((label, labelIndex) => (
                <li
                  key={labelIndex}
                  style={{ color: this.props.colors.textColor }}
                >
                  {label}
                </li>
              ))}
            </ul>
          </div>
          <div className="main">
            <ul className="toolbar">
              {this.props.params.toolbarButtons.map((button, buttonIndex) => (
                <li
                  key={buttonIndex}
                  style={{
                    backgroundColor: this.props.colors.backgroundColor,
                    borderColor: this.props.colors.borderColor,
                    color: this.props.colors.textColor
                  }}
                />
              ))}
            </ul>
            <ul className="conversations">
              {this.props.params.conversations.map(
                (conversation, conversationIndex) => (
                  <li
                    key={conversationIndex}
                    className={conversation.unread && "unread"}
                    style={{
                      backgroundColor: this.props.colors
                        .conversationsBackgroundColor,
                      borderBottomColor: this.props.colors.borderColor,
                      borderLeftColor: conversation.active
                        ? this.props.colors.primaryColor
                        : this.props.colors.borderColor,
                      borderTopColor:
                        conversationIndex === 0 &&
                        this.props.colors.borderColor,
                      color: this.props.colors.textColor
                    }}
                  >
                    <span
                      className="star"
                      style={{
                        visibility: conversation.starred || "hidden"
                      }}
                    >
                      ★{" "}
                    </span>
                    {conversation.title}
                  </li>
                )
              )}
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

export default Preview;
