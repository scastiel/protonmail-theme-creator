import React, { Component, PropTypes } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import {
  colorDefinitions,
  themes,
  previewParameters
} from "../../ressources/themes";
import PreviewContainer from "../preview-container/PreviewContainer";
import ThemeSelector from "../theme-selector/ThemeSelector";
import ColorsSelector from "../colors-selector/ColorsSelector";
import ThemeCSSViewer from "../theme-css-viewer/ThemeCSSViewer";
import * as colorThemesActions from "../../actions/color-themes-actions";

import "./App.css";

class App extends Component {
  static propTypes = {
    selectedThemeId: PropTypes.string,
    colors: PropTypes.objectOf(PropTypes.string).isRequired,
    actions: PropTypes.object.isRequired,
    createThemeCSS: PropTypes.func.isRequired
  };
  static defaultProps = {
    css: ""
  };
  componentDidMount() {
    document.body.className += " loaded";
  }
  render() {
    return (
      <div>
        <div className="row">
          <div className="col">
            <PreviewContainer
              params={previewParameters}
              colors={this.props.colors}
            />
          </div>
        </div>
        <div className="row">
          <div className="col">
            <div className="card">
              <div className="card-block">
                <ThemeSelector
                  themes={themes}
                  value={this.props.selectedThemeId}
                  changeTheme={this.props.actions.changeTheme}
                />
                <ColorsSelector
                  colorDefinitions={colorDefinitions}
                  colors={this.props.colors}
                  onChange={this.props.actions.changeColors}
                />
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col">
            <ThemeCSSViewer
              css={this.props.createThemeCSS(this.props.colors)}
            />
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state, props) {
  const selectedThemeId = state.colorThemes.selectedThemeId;
  const colors = state.colorThemes.colors;
  const createThemeCSS = state.colorThemes.createThemeCSS;
  return { selectedThemeId, colors, createThemeCSS };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(colorThemesActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
