import React, { Component, PropTypes } from "react";
import ColorPicker from "../color-picker/ColorPicker";
import "./ColorsSelector.css";

export default class ColorsSelector extends Component {
  static propTypes = {
    colorDefinitions: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string,
        name: PropTypes.string
      })
    ).isRequired,
    colors: PropTypes.objectOf(PropTypes.string).isRequired,
    onChange: PropTypes.func
  };
  changeColor(colorId, color) {
    const newColors = { ...this.props.colors, [colorId]: color };
    if (typeof this.props.onChange === "function") {
      this.props.onChange(newColors);
    }
  }
  render() {
    return (
      <div className="colors-selector">
        <p>Customize the colors of your theme as you want:</p>
        <ul>
          {this.props.colorDefinitions.map(colorDefinition => (
            <li key={colorDefinition.id}>
              <ColorPicker
                color={this.props.colors[colorDefinition.id]}
                onChange={color => this.changeColor(colorDefinition.id, color)}
              />
              &nbsp; <span className="color-name">{colorDefinition.name}</span>
            </li>
          ))}
        </ul>
      </div>
    );
  }
}
