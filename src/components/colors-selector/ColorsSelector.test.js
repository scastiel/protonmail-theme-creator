import React, { Component } from "react";
import ReactDOM from "react-dom";
import ReactTestUtils from "react-addons-test-utils";
import ColorPicker from "../color-picker/ColorPicker";
import { expect } from "chai";

jest.mock(
  "../color-picker/ColorPicker",
  () =>
    class ColorPickerMock extends Component {
      onChange(event) {
        event.preventDefault();
        this.props.onChange(event.target.value);
      }
      render() {
        return (
          <input
            type="text"
            value={this.props.color}
            onChange={e => this.onChange(e)}
          />
        );
      }
    }
);

const ColorsSelector = require("./ColorsSelector").default;

const colorDefinitions = [
  { id: "header", name: "Header background color" },
  { id: "body", name: "Body text color" }
];
const colors = {
  header: "#000",
  body: "#FFF"
};

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(
    <ColorsSelector colorDefinitions={colorDefinitions} colors={colors} />,
    div
  );

  const colorsSelector = div.querySelector(".colors-selector");
  expect(colorsSelector).to.exist;
  const colorsList = colorsSelector.querySelector("ul");
  expect(colorsList).to.exist;
  const colorListElements = colorsList.querySelectorAll("li");
  const colorNames = Array.from(colorListElements).map(
    e => e.querySelector(".color-name").innerHTML
  );
  expect(colorNames).to.deep.eql(colorDefinitions.map(c => c.name));
});

it("calls onChange function when a color is changed", () => {
  const div = document.createElement("div");
  const onChange = jest.fn();
  ReactDOM.render(
    <ColorsSelector
      colorDefinitions={colorDefinitions}
      colors={colors}
      onChange={onChange}
    />,
    div
  );

  const colorInput = div.querySelector("li:nth-of-type(1) input");
  ReactTestUtils.Simulate.change(colorInput, { target: { value: "#123" } });
  expect(onChange.mock.calls).to.have.lengthOf(1);
  expect(onChange.mock.calls[0][0]).to.deep.equal({
    header: "#123",
    body: "#FFF"
  });
});
