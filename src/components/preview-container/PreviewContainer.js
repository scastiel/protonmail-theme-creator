import React, { Component } from "react";
import Preview from "../preview/Preview";
import { previewParameters } from "../../ressources/themes";
import "./PreviewContainer.css";

export default class PreviewContainer extends Component {
  render() {
    return (
      <div className="card preview-container">
        <Preview params={previewParameters} colors={this.props.colors} />
      </div>
    );
  }
}
