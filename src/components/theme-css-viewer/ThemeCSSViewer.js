import React, { Component, PropTypes } from "react";
import CopyToClipboard from "react-copy-to-clipboard";
import "./ThemeCSSViewer.css";

export default class ThemeCSSViewer extends Component {
  static propTypes = {
    css: PropTypes.string.isRequired
  };
  constructor(props) {
    super(props);
    this.state = { copied: false };
  }
  setCopied() {
    this.setState({ copied: true });
    setTimeout(() => {
      this.setState({ copied: false });
    }, 2000);
  }
  render() {
    return (
      <div className="card">
        <div className="card-header d-flex justify-content-between">
          <span>Generated CSS</span>
          <CopyToClipboard
            text={this.props.css}
            onCopy={() => this.setCopied()}
          >
            <a href="#" onClick={e => e.preventDefault()}>
              {this.state.copied ? "Copied!" : "Copy"}
            </a>
          </CopyToClipboard>
        </div>
        <pre className="generated-css">
          <code>{this.props.css}</code>
        </pre>
      </div>
    );
  }
}
