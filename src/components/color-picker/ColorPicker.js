import React from "react";
import "./ColorPicker.css";
import { SketchPicker } from "react-color";

class ColorPicker extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      color: props.color,
      displayColorPicker: false
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ color: nextProps.color });
  }

  handleClick = () => {
    this.setState({ displayColorPicker: !this.state.displayColorPicker });
  };

  handleClose = () => {
    this.setState({ displayColorPicker: false });
  };

  handleChange = color => {
    this.setState({ color: color.hex });
    if (typeof this.props.onChange === "function") {
      this.props.onChange(color.hex);
    }
  };

  render() {
    return (
      <div className="color-picker">
        <div className="swatch" onClick={this.handleClick}>
          <div
            className="color"
            style={{ backgroundColor: this.state.color }}
          />
        </div>
        {this.state.displayColorPicker && (
          <div className="popover">
            <div className="cover" onClick={this.handleClose} />
            <SketchPicker
              color={this.state.color}
              onChange={this.handleChange}
            />
          </div>
        )}
      </div>
    );
  }
}

export default ColorPicker;
